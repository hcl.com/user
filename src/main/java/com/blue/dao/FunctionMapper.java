package com.blue.dao;

import com.blue.bean.Function;
import com.blue.dto.FunctionResponse;
import com.blue.utils.ParamMap;

import java.util.List;

public interface FunctionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Function record);

    int insertSelective(Function record);

    Function selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Function record);

    int updateByPrimaryKey(Function record);

    List<FunctionResponse> listByMap(ParamMap paramMap);

    Function getByMap(ParamMap paramMap);

    List<Function> listByPage(ParamMap paramMap);

    int countByPage(ParamMap paramMap);

}