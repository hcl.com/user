package com.blue.dao;

import com.blue.bean.User;
import com.blue.utils.ParamMap;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User get(ParamMap paramMap);
}