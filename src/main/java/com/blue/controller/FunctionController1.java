//package com.blue.controller;
//
//import com.usercard.common.SessionUtils;
//import com.usercard.dao.Function;
//import com.usercard.domain.MemberWelfare;
//import com.usercard.dto.FunctionRequest;
//import com.usercard.dto.StoreActionConfigRequest;
//import com.usercard.service.impl.FunctionService;
//import com.usercard.service.impl.MemberWelfareService;
//import com.usercard.utils.ParamMap;
//import com.usercard.utils.Response;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.List;
//
//@Controller
//@RequestMapping("/api/function")
//public class FunctionController1 extends BaseController {
//
//	@Autowired
//	private FunctionService functionService;
//
//	@Autowired
//	private MemberWelfareService memberWelfareService;
//
//	/**
//	 * 查询会员福利
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/welfare_list", method = RequestMethod.GET)
//	public Response findByWelfareList(HttpServletRequest request){
//		ParamMap param = ParamMap.init(request);
//		param.put(STORE_ID, super.getStoreId());
//
//		return memberWelfareService.findByList(param);
//	}
//
//	/**
//	 * 添加会员福利
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/addWelfare", method = RequestMethod.POST)
//	public Response addWelfare(MemberWelfare memberWelfare){
//		memberWelfare.setStoreId(super.getStoreId());
//		return memberWelfareService.add(memberWelfare);
//	}
//
//	/**
//	 * 修改会员福利
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/updateWelfare", method = RequestMethod.POST)
//	public Response updateWelfare(MemberWelfare memberWelfare){
//		memberWelfare.setStoreId(super.getStoreId());
//		System.out.println(memberWelfare.getDetailed());
//		System.out.println(memberWelfare.getPosition());
//		System.out.println(memberWelfare.getName());
//		System.out.println(memberWelfare.getId());
//
//		return memberWelfareService.update(memberWelfare);
//	}
//
//	/**
//	 * 删除会员福利
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/deleteWelfare", method = RequestMethod.POST)
//	public Response deleteWelfare(Long id){
//		return memberWelfareService.delete(id);
//	}
//
//
//	/**
//	 * 角色动作配置
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/role_action", method = RequestMethod.POST)
//	public Response roleAction(HttpServletRequest request) {
//		Response response = Response.newResponse();
//
//		ParamMap param = ParamMap.init(request);
//		functionService.roleAction(param);
//
//		return response.OK();
//	}
//
//
//	/**
//	 * 品牌权限动作配置
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/store_action", method = RequestMethod.POST)
//	public Response storeAction(HttpServletRequest request) {
//		Response response = Response.newResponse();
//
//		ParamMap param = ParamMap.init(request);
//		functionService.storeAction(param);
//
//		return response.OK();
//	}
//
//	/**
//	 * 查询菜单
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/menus", method = RequestMethod.GET)
//	public Response functions() {
//		Response response = Response.newResponse();
//
//		Long employeeId = SessionUtils.get().getId();
//		List<Function> functions = functionService.findByEmployeeId(employeeId);
//
//		return response.ok(functions);
//	}
//
//	/**
//	 * 角色权限分配
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/list", method = RequestMethod.GET)
//	public Response findByRole(HttpServletRequest request) {
//		ParamMap param = ParamMap.init(request);
//		return this.functionService.findByRole(param);
//	}
//
//	/**
//	 * 角色权限分配(加入品牌权限控制筛选后)
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/list/v2", method = RequestMethod.GET)
//	public Response findByRoleV2(HttpServletRequest request) {
//		ParamMap param = ParamMap.init(request);
//		Long storeId = super.getParentStoreId();
//		param.set("storeId", storeId);
//		return this.functionService.findByRoleV2(param);
//	}
//
//	/**
//	 * 角色权限分配(加入品牌权限控制筛选后)
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/list/v3", method = RequestMethod.GET)
//	public Response findByRoleV3(HttpServletRequest request) {
//		ParamMap param = ParamMap.init(request);
//		return this.functionService.findByRoleV2(param);
//	}
//	/**
//	 * 品牌权限分配
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/list/store/{id}", method = RequestMethod.GET)
//	public Response findByStore(@PathVariable Long id) {
//		return this.functionService.findActionByStore(id);
//	}
//
//
//	/**
//	 * 品牌权限分配
//	 * @return
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/list/store/proxy/{id}", method = RequestMethod.GET)
//	public Response findByStorePorxy(@PathVariable Long id) {
//
//		//ParamMap param = ParamMap.init(request);
//		//param.put("roleId", super.getLogin().getRole().getId());
//		return this.functionService.findActionByStoreProxy(super.getStoreId(),id);
//	}
//
//	/**
//	 * 查询一级菜单
//	 *
//	 */
//
//	@ResponseBody
//	@RequestMapping(value = "/findByPage", method = RequestMethod.GET)
//	public Response findByPage(HttpServletRequest request) {
//							ParamMap param = ParamMap.init(request);
//							param.convertsLike(KEYWORD);
//
//		  return this.functionService.findByPage(param);
//	}
//
//	/**
//	 * 查询一级菜单code最大值
//	 *
//	 */
//
//	@ResponseBody
//	@RequestMapping(value = "/findMaxCode", method = RequestMethod.GET)
//	public Response findMaxCode(HttpServletRequest request) {
//								ParamMap param = ParamMap.init(request);
//
//		     return this.functionService.findMaxCode(param);
//	}
//
//	/**
//	 * 查询一级菜单code最大值
//	 *
//	 */
//
//	@ResponseBody
//	@RequestMapping(value = "/findpIdCount", method = RequestMethod.GET)
//	public Response findpIdCount(HttpServletRequest request) {
//					ParamMap param = ParamMap.init(request);
//
//			return this.functionService.findpIdCount(param);
//	}
//
//
//	/**
//	 * 添加菜单
//	 *
//	 */
//
//	@ResponseBody
//    @RequestMapping( method = RequestMethod.POST)
//    public Response add(FunctionRequest functionRequest) {
//
//		  return this.functionService.add(functionRequest);
//	}
//
//	/**
//	 * 修改菜单
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/update", method = RequestMethod.PUT)
//    public Response updateMain(FunctionRequest functionRequest) {
//						Response response = Response.newResponse();
//						this.functionService.update(functionRequest);
//
//            return response.OK();
//    }
//
//	/***
//	 * 删除function
//	 *
//	 */
//
//	@ResponseBody
//	@RequestMapping(value="/{id}",method = RequestMethod.DELETE)
//	public Response remove(@PathVariable Integer id) {
//							Response response = Response.newResponse();
//							this.functionService.remove(id);
//
//		    return response.OK();
//	}
//
//	@ResponseBody
//	@RequestMapping(value = "/defaultlist", method = RequestMethod.GET)
//	public Response defaultlist(HttpServletRequest request) {
//		ParamMap param = ParamMap.init(request);
//		return this.functionService.defaultlist(param);
//	}
//
//	@ResponseBody
//	@RequestMapping(value = "/updateDefaultStoreAction", method = RequestMethod.PUT)
//    public Response updateDefaultStoreAction(StoreActionConfigRequest storeActionConfigRequest) {
//        Response response = Response.newResponse();
//        this.functionService.updateDefaultStoreAction(storeActionConfigRequest);
//
//        return response.OK();
//    }
//	@ResponseBody
//	@RequestMapping(value = "/existsByCode", method = RequestMethod.POST)
//	public Response isExistsByCode(HttpServletRequest request){
//
//    	ParamMap param = ParamMap.init(request);
//    	param.put("id", param.getString("id"));
//    	param.put("code", param.getString("editcode"));
//
//    	Boolean is = this.functionService.isExistsByCode(param);
//    	return Response.set("valid", !is);
//	}
//
//	@ResponseBody
//	@RequestMapping(value = "/login_out", method = RequestMethod.GET)
//	public Response loginOutPage(HttpServletRequest request) {
//		SessionUtils.clear(request);
//		return Response.newResponse().OK();
//	}
//}
