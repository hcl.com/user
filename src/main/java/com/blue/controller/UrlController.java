package com.blue.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UrlController extends BaseController{

    @RequestMapping("/")
    public String welcome() {
        logger.info("1","2");
        return "login";
    }

    @RequestMapping("/index")
    public String index() {
        
        return "/index";
    }

    @RequestMapping("/login")
    public String login() {
        
        return "login";
    }

    @RequestMapping("/a")
    public String a() {
        
        return "iview";
    }

    @RequestMapping("/b")
    public String b() {
        
        return "b";
    }

    @RequestMapping("/iframe")
    public String iframe() {
        
        return "iframe";
    }

    @RequestMapping("/function_list")
    public String function_list() {
        
        return "function_list";
    }

    @RequestMapping("/son_function_list")
    public String son_function_list() {
        
        return "son_function_list";
    }

    @RequestMapping("/iview")
    public String function_list(HttpServletRequest httpServletRequest) {
        return "iview";
    }

    @RequestMapping("/ly_table")
    public String lg_test(HttpServletRequest httpServletRequest) {
        return "ly_table";
    }

    @RequestMapping("/ly_form")
    public String ly_form(HttpServletRequest httpServletRequest) {
        return "ly_form";
    }
    @RequestMapping("/lyIndex")
    public String lyIndex(HttpServletRequest httpServletRequest) {
        return "lyIndex";
    }
    @RequestMapping("/lteIndex")
    public String lteIndex(HttpServletRequest httpServletRequest) {
        return "lteIndex";
    }
    @RequestMapping("/test")
    public String test(HttpServletRequest httpServletRequest) {
        return "test";
    }
}
