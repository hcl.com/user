package com.blue.controller;

import com.blue.bean.Function;
import com.blue.bean.User;
import com.blue.dao.FunctionMapper;
import com.blue.service.FunctionService;
import com.blue.utils.ErrorEnum;
import com.blue.utils.ParamMap;
import com.blue.utils.Response;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.print.DocFlavor;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(value = "/api/function")
public class FunctionController {
    private static final Logger logger = LoggerFactory.getLogger(FunctionController.class);

    @Autowired
    private FunctionService functionService;
    @Autowired
    private FunctionMapper functionDao;

    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Response getUsers3(User u, HttpServletRequest httpServletRequest) {
        Response response = Response.newResponse();

        User user = (User) httpServletRequest.getSession().getAttribute("user");
        if (user == null) {
            return response.setError(ErrorEnum.PARAM_ERROR).setMessage("登录失效");
        }
        return functionService.listByUser(user);
    }

    @ResponseBody
    @RequestMapping(value = "/findByPage", method = RequestMethod.GET)
    public Response findByPage(HttpServletRequest request) {
        ParamMap param = ParamMap.init(request);
        param.convertsLike("keyword");
        if(param.getInt("page")!=null){
            param.setPages(param.getInt("page")-1,param.getInt("limit"));
        }

        Response response = Response.newResponse();
        Integer count = this.functionDao.countByPage(param);
        List<Function> list = this.functionDao.listByPage(param);

        return response.setResults(count, list);
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Response delete(@PathVariable Integer id, HttpServletRequest request) {
        Response response = Response.newResponse();
        this.functionDao.deleteByPrimaryKey(id);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Response insert(Function function, HttpServletRequest request) {
        Response response = Response.newResponse();
        this.functionDao.insertSelective(function);
        return response;
    }

    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Response update(Function function, HttpServletRequest request) {
        Response response = Response.newResponse();
        this.functionDao.updateByPrimaryKeySelective(function);
        return response;
    }
}
