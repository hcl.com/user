package com.blue.service;

import com.blue.bean.User;
import com.blue.dao.FunctionMapper;
import com.blue.dto.FunctionResponse;
import com.blue.utils.ErrorEnum;
import com.blue.utils.ParamMap;
import com.blue.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FunctionServiceImpl implements FunctionService {
    private Response response;
    @Autowired
    private FunctionMapper functionMapper;

    @Override
    public Response listByUser(User user) {
        response = Response.newResponse();
        ParamMap paramMap = ParamMap.init("roleId",user.getRoleId());
        List<FunctionResponse> plist = functionMapper.listByMap(paramMap);
        if (plist != null) {
            for (FunctionResponse functionResponse : plist) {

                paramMap.remove("roleId");
                paramMap.add("pId",functionResponse.getId());
                List<FunctionResponse> slist = functionMapper.listByMap(paramMap);
                functionResponse.setFunctions(slist);
            }
        }
        return response.ok(plist);
    }
}
