package com.blue.service;

import com.blue.bean.User;
import com.blue.utils.Response;

public interface FunctionService {

    Response listByUser(User user);
}
