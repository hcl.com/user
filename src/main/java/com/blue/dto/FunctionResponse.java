package com.blue.dto;

import com.blue.bean.Function;

import java.util.List;

public class FunctionResponse extends Function {

    private List<FunctionResponse> functions;

    public List<FunctionResponse> getFunctions() {
        return functions;
    }

    public void setFunctions(List<FunctionResponse> functions) {
        this.functions = functions;
    }
}
