<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="common.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>登录</title>
    <%--<link rel="stylesheet" type="text/css" href="${contextPath }/css/login/style_log.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${contextPath }/css/login/style.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${contextPath }/css/login/userpanel.css">--%>
    <%--<link rel="stylesheet" type="text/css" href="${contextPath }/css/login/jquery.ui.all.css">--%>

    <style>
        body {
            <%--background: url(${contextPath }/image/bg/1.jpg) #f8f6e9;--%>
        }

        .mycenter {
            margin-top: 100px;
            margin-left: auto;
            margin-right: auto;
            height: 350px;
            width: 500px;
            padding: 5%;
            padding-left: 5%;
            padding-right: 5%;
        }

        .mycenter mysign {
            width: 440px;
        }

        .mycenter input, checkbox, button {
            margin-top: 2%;
            margin-left: 10%;
            margin-right: 10%;
        }

        .mycheckbox {
            margin-top: 10px;
            margin-left: 40px;
            margin-bottom: 10px;
            height: 10px;
        }
    </style>
</head>

<body>
<form class="form-horizontal col-sm-offset-3 col-md-offset-3" id="loginForm">
    <div class="mycenter">
        <div class="mysign">
            <div class="col-lg-11 text-center text-info">
                <h2>登录</h2>
            </div>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="username" placeholder="请输入账户名" required autofocus/>
            </div>
            <div class="col-lg-10"></div>
            <div class="col-lg-10">
                <input type="password" class="form-control" name="password" placeholder="请输入密码" required autofocus/>
            </div>
            <div class="col-lg-10"></div>
            <div class="col-lg-10 mycheckbox checkbox">
                <input type="checkbox">记住密码</input>
            </div>
            <div class="col-lg-10"></div>
            <div class="col-lg-10">
                <button type="button" class="btn btn-success col-lg-12">登录</button>
            </div>
        </div>
    </div>
</form>
</body>
</html>
</html>
<script type="text/javascript">
</script>