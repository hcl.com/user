<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ include file="common.jsp" %>--%>
<%--bootstrap--%>


<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>Bootstrap 实例 - 模态框（Modal）插件</title>

    <%--<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>

    <%--<c:set var="contextPath" scope="application" value="${pageContext.request.contextPath }"></c:set>--%>

<%--&lt;%&ndash;layui&ndash;%&gt;--%>
    <%--<script src="${contextPath }/js/angular.min.js"></script>--%>


    <title>angular</title>

    <script  src="../../plugins/jquery-3.2.1/jquery-3.2.1.min.js"></script>

    <%--<script  src="../../plugins/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>--%>
    <%--<link href="../../plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">--%>

    <link href="${contextPath }/plugins/layui-v2.1.5/layui/css/layui.css" rel="stylesheet"></link>
    <script  src="../../plugins/layui-v2.1.5/layui/layui.js"></script>
</head>
<body>

<h2>创建模态框（Modal）</h2>
<!-- 按钮触发模态框 -->
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">开始演示模态框</button>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">模态框（Modal）标题</h4>
            </div>
            <div class="modal-body">在这里添加一些文本</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary">提交更改</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<button id="btn1">开始演示模态框</button>
<button id="btn2">msg</button>
<div class="layui-row layui-col-md-3" style="background: red">asf</div>

<%--<input type="text" class="" value="asdf" id="asdf"/>--%>
</body>
</html>
<script>
    layui.use(['layer'], function(){
        var layer=layui.layer;
        //在这里面输入任何合法的js语句
        $('#btn1').click(function () {
            layer.open({
                type: 1 //Page层类型
                ,area: ['50%', '20%']
                ,title: '你好，layer。'
//                ,shade: 0.5 //遮罩透明度
//                ,maxmin: true //允许全屏最小化
//                ,anim: 2 //0-6的动画形式，-1不开启
                ,content: '<input type="text" class="" value="asdf" id="input1"/>' +
                '<button id="btn2">获取值</button>'
            });

            $('#btn2').click(function () {
                layer.msg($('#input1').val());
            })
        })
        $('#btn2').click(function () {
            layer.msg('灵活运用offset', {
                offset: 't',
                anim: 6
            });
        })
    })

</script>