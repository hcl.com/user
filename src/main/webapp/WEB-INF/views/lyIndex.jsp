<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" scope="application" value="${pageContext.request.contextPath }"></c:set>

<%--<script src="${contextPath }/js/commons.js"></script>--%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>liblue管理系统</title>

    <style type="text/css">
        <%--这个是设置a标签的默认状态去除下划线--%>
        a {
            text-decoration: none;
        }

        /*这个是设置a标签的访问过后的状态去除下划线*/
        a:visited {
            text-decoration: none;
        }

        /*这个是设置a标签的鼠标覆盖状态去除下划线*/
        a:hover {
            text-decoration: none;
        }

        /*这个是设置a标签的活跃状态去除下划线*/
        a:active {
            text-decoration: none;
        }

        /* 指正常的未被访问过的链接*/
        a:link {
            text-decoration: none;
        }
    </style>
    <%--jquery--%>
    <script  src="../../plugins/jquery-3.2.1/jquery-3.2.1.min.js"></script>
    <%--bootstrap--%>
    <script  src="${contextPath }/plugins/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link href="${contextPath }/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">

    <%--引入toastr--%>
    <link href="${contextPath }/plugins/toastr/toastr.css" rel="stylesheet"/>
    <script src="${contextPath }/plugins/toastr/toastr.js"></script>

    <%--引入fontawesome--%>
    <link href="${contextPath }/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <%--layui--%>
    <link href="${contextPath }/plugins/layui/css/layui.css" rel="stylesheet">
    <script src="${contextPath }/plugins/layui/layui.js"></script>

    <script src="${contextPath }/js/commons.js"></script>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">管理系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <%--<li class="layui-nav-item"><a href="http://www.layui.com/demo/layuiAdmin.html">控制台</a></li>--%>
            <%--<li class="layui-nav-item"><a href="http://www.layui.com/demo/layuiAdmin.html">商品管理</a></li>--%>
            <%--<li class="layui-nav-item"><a href="http://www.layui.com/demo/layuiAdmin.html">用户</a></li>--%>
            <%--<li class="layui-nav-item">--%>
            <%--<a href="javascript:;">其它系统<span class="layui-nav-more"></span></a>--%>
            <%--<dl class="layui-nav-child">--%>
            <%--<dd><a href="http://www.layui.com/demo/layuiAdmin.html">邮件管理</a></dd>--%>
            <%--<dd><a href="http://www.layui.com/demo/layuiAdmin.html">消息管理</a></dd>--%>
            <%--<dd><a href="http://www.layui.com/demo/layuiAdmin.html">授权管理</a></dd>--%>
            <%--</dl>--%>
            <%--</li>--%>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="../../image/1.jpg" class="layui-nav-img">贤心
                    <span class="layui-nav-more"></span>
                </a>
                <dl class="layui-nav-child layui-anim layui-anim-upbit">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                    <dd><a href="#">退出</a></dd>
                </dl>
            </li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">

        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test" id="ulLeft">
                <li class="layui-nav-item"><a href="lyIndex">首页</a></li>
                <li class="layui-nav-item"><a href="javascript:;"><i class="fa fa-home"></i>&nbsp;&nbsp;菜单管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="function_list"><i class="fa fa-address-card"></i>&nbsp;&nbsp;菜单管理1</a></dd>
                        <dd><a href="function_list"><i class="fa fa-address-card"></i>&nbsp;&nbsp;菜单管理1</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="javascript:;"><i
                        class="fa fa-address-book-o"></i>&nbsp;&nbsp;会员管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="login"><i class="fa fa-bandcamp"></i>&nbsp;&nbsp;会员管理1</a></dd>
                        <dd><a href="ly_table"><i class="fa fa-bath"></i>&nbsp;&nbsp;会员管理2</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>

    <div id="divRight" class="layui-body" style="padding: 15px;">
        <!-- 内容主体区域 -->
        <h2>创建模态框（Modal）</h2>
        <!-- 按钮触发模态框 -->
        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">开始演示模态框</button>
        <!-- 模态框（Modal） -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">模态框（Modal）标题</h4>
                    </div>
                    <div class="modal-body">在这里添加一些文本</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        <button type="button" class="btn btn-primary">提交更改</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal -->
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域asfd
    </div>
</div>

</body>
</html>

<script type="text/javascript">
    var element;
    var layer;


    var id;
    $(function () {
        getData()

//        $("li[name='fatherMenu']").click(function () {
//            var cls = $(this).attr('class')
//            consoleLog('点击', cls)
//            cleanClass()
//            var mr = 'layui-nav-item';
//            var dj = 'layui-nav-item layui-nav-itemed';
//            if (cls == mr) {
//                $(this).attr('class', dj)
//            }
//        })
//
//        function cleanClass() {
//            var mr = 'layui-nav-item';
//            $("li[name='fatherMenu']").each(function (i, v) {
//                $(this).attr('class', mr)
//            })
//        }

//        $("a[name='sonMenu']").click(function (e) {
//            consoleLog('子菜单点击', e)
//            var url = $(this).attr('href')
//            $('#divRight').load(url)
//
//            e.preventDefault();
//        })

        function getData() {
            $.get('./api/function/list', function (r) {
                if (r.code == 200) {
//                $('#ulLeft').empty();
                    var html = '<li class="layui-nav-item"><a href="lyIndex">首页</a></li>';
                    $.each(r.data, function (i, v) {
                        if (v.functions && v.functions.length > 0) {
                            //有子菜单
                            html += '<li class="layui-nav-item" name="fatherMenu"><a href="javascript:;"  name="fatherMenu"><i class="' + v.icon + '"></i>&nbsp;&nbsp;' + v.title + '</a>';
                            html += '<dl class="layui-nav-child" >';
                            $.each(v.functions, function (i1, v1) {
                                html += '<dd><a href="' + v1.url + '" name="sonMenu"><i class="' + v1.icon + '"></i>&nbsp;&nbsp;' + v1.title + '</a></dd>'
                            })
                            html += '</dl></li>';
                        } else {
                            //无子菜单
                            html += '<li class="layui-nav-item" name="fatherMenu"><a href="javascript:;" name="fatherMenu"><i class="' + v.icon + '">&nbsp;&nbsp;' + v.title + '</a></li>';
                        }
                    })
                    $('#ulLeft').html(html);



                    $("a[name='sonMenu']").click(function (e) {
                        var url = $(this).attr('href')
                        $('#divRight').load(url)

                        e.preventDefault();
                    })

//
                    function cleanClass() {
                        var mr = 'layui-nav-item';
                        $("li[name='fatherMenu']").each(function (i, v) {
                            $(this).attr('class', mr)
                        })
                    }

                    layui.use('layer', function(){
                        var layer = layui.layer ;
                        layui.use(['element'], function () {
                            element = layui.element;
                        });
                    });

                } else {
                    toastrError(r.message)
                }
            })
        }

    })

</script>
