<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="common.jsp" %>
<%--<script src="${contextPath }/js/commons.js"></script>--%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>layui表格</title>

</head>
<body>
<%--<table class="layui-table" lay-data="{height:315, url:'http://www.layui.com/demo/table/user/', page:true, id:'test'}" lay-filter="test">--%>
<%--<thead>--%>
<%--<tr>--%>
<%--<th lay-data="{field:'id', width:80, sort: true}">ID</th>--%>
<%--<th lay-data="{field:'name', width:80}">用户名</th>--%>
<%--<th lay-data="{field:'age', width:80, sort: true}">性别</th>--%>
<%--</tr>--%>
<%--</thead>--%>
<%--</table>--%>
<hr>
<div class="layui-form-item">
    <div class="layui-input-inline">
        <input type="text" id="filter" name="filter" value="" lay-verify="" placeholder="请输入搜索内容" autocomplete="off"
               class="layui-input">
    </div>
    <div class="layui-input-inline">
        <button class="layui-btn" id="search" name="search">
            <i class="layui-icon"></i>搜索
        </button>
        <button class="layui-btn" id="batch_d" name="search">
            <i class="layui-icon"></i>批量删除
        </button>
    </div>
</div>
<div>
    <table lay-filter="customer1">
        <colgroup>
            <col width="8">
            <col width="8%">
            <col width="9%">
            <col width="9%">
            <col width="9%">
            <col width="9%">
            <col width="9%">
            <col width="28%">
        </colgroup>
        <thead>
        <tr style="background-color:#428bca;color:#FFF;">
            <th class="td_center">菜单编号</th>
            <th class="td_center">菜单类型</th>
            <th class="td_center">图标</th>
            <th class="td_center">菜单名称</th>
            <th class="td_center">菜单地址</th>
            <th class="td_center">排序字段</th>
            <th class="td_center">所属范围</th>
            <th class="td_center">操作</th>
        </tr>
        </thead>
        <tbody id="customer">

        </tbody>
    </table>
</div>
</body>
</html>

<%--@*工具栏模版*@--%>
<script type="text/html" id="bar">
    <a class="layui-btn layui-btn-mini layui-btn-normal" title="详细信息编辑" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-mini" title="删除" lay-event="delete">删除</a>
</script>
<%--@*日期模板*@--%>
<script type="text/html" id="dateTpl">
    {{ layui.laytpl.fn(d.editdate) }}
</script>
<%--@*执行状态模板*@--%>
<script type="text/html" id="statusTpl">
    {{# var status=d.status }}
    {{# if (status=='1'){ }}
    <span class="layui-badge layui-bg-blue" style="font-size:13px">启用</span>
    {{# } else{ }}
    <span class="layui-badge" style="font-size:13px">禁用</span>
    {{# } }}
</script>
<%--@*菜单类型模板*@--%>
<script type="text/html" id="typeTpl">
    {{# var status=d.pId }}
    {{# if (status==0){ }}
    <span class="layui-badge layui-bg-blue" style="font-size:13px">一级菜单</span>
    {{# } else{ }}
    <span class="layui-badge" style="font-size:13px">二级菜单</span>
    {{# } }}
</script>
<script type="text/javascript">
    layui.use(['layer', 'table'], function () {
        var table = layui.table;
        var layer = layui.layer;
        layui.laytpl.fn = function (value) {
            //json日期格式转换为正常格式
            var date = new Date(parseInt(value.replace("/Date(", "").replace(")/", ""), 10));
            var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
            var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            return date.getFullYear() + "-" + month + "-" + day;
        }
        //--------------方法渲染TABLE----------------
        var tableIns = table.render({
            elem: '#customer'
            , id: 'customer'
            , url: '/api/function/findByPage'
            , cols: [[
                {checkbox: true, LAY_CHECKED: false, align: 'center'} //其它参数在此省略
                , {field: 'id', align: 'center'}
                , {field: 'pId', align: 'center', templet: "#typeTpl"}
                , {field: 'company', align: 'center'}
                , {field: 'descr', align: 'center'}
                , {field: 'address1', align: 'center'}
                , {field: 'contact', align: 'center'}
                , {field: 'status', align: 'center', templet: "#statusTpl"}
                , {field: 'editwho', align: 'center'}
                , {field: 'editdate', align: 'center', templet: "#dateTpl"}
                , {fixed: 'right', align: 'center', toolbar: '#bar'}
            ]]
            , page: true
            , limits: [5, 10, 40, 60, 80]
            , limit: 3 //默认采用10
            , response: {
                statusName: 'code' //数据状态的字段名称，默认：code
                , statusCode: 200 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 'total' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                consoleLog('res', res)
                consoleLog('curr', curr)
                consoleLog('count', count)
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                //console.log(res);
                //得到当前页码
                //console.log(curr);
//                $("#curPageIndex").val(curr);
                //得到数据总量
                //console.log(count);
            }
        });

        //#region --------------搜索----------------
        $("#search").click(function () {
            var strFilter = $("#filter").val();
            tableIns.reload({
                where: {
                    keywork: strFilter
                }
            });
        });
        //#region --------------搜索----------------
        $("#batch_d").click(function () {
            var checkStatus = table.checkStatus('customer');//选中的数组
            consoleLog('checkStatus', checkStatus)
            var count = checkStatus.data.length;//选中的行数
            consoleLog('count', count)
            if (count > 0) {
                parent.layer.confirm('确定要删除所选客户信息', {icon: 3}, function (index) {
                    var data = checkStatus.data; //获取选中行的数据
                    var customer_code = [];
                    for (var i = 0; i < data.length; i++) {
                        customer_code.push(data[i].id)
                    }
                    customer_code.join(',')
                    console.log(customer_code);
                });
            }
            else {
                parent.layer.msg("请至少选择一条数据", {icon: 5, shade: 0.4, time: 1000});
            }
        });
        //#endregion

        //#endregion
        //工具条事件监听
        table.on('tool(customer1)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            consoleLog('data', data)
            var layEvent = obj.event; //获得 lay-event 对应的值
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            var customer_code = data.customer_code;
            if (layEvent === 'edit') { //编辑
                parent.layer.open({
                    type: 2,
                    title: '编辑客户信息',
                    shade: 0.4,  //阴影度
                    fix: false,
                    shadeClose: false,
                    maxmin: false,
                    area: ['1260px;', '645px;'],    //窗体大小（宽,高）
                    content: data,
                    success: function (layero, index) {
                        var body = layer.getChildFrame('body', index); //得到子页面层的BODY
                        body.find('#hidValue').val(index); //将本层的窗口索引传给子页面层的hidValue中
                    },
                    end: function () {
                        var handle_status = $("#handle_status").val();
                        console.log(handle_status);
                        if (handle_status == 'ok') {
                            parent.layer.msg('修改成功', {icon: 1, shade: 0.4, time: 1000});
                            $("#search").click();
                            $("#handle_status").val('');
                        }
                        else if (handle_status == "error") {
                            parent.layer.msg('修改失败', {icon: 5, shade: 0.4, time: 1000});
                        }
                    }
                });
            }
            if (layEvent === 'delete') { //编辑
                layer.confirm('确认删除？', {
                    btn: ['确认', '取消'] //可以无限个按钮
                }, function (index) {
                    //按钮【按钮一】的回调
                    layer.close(index);
                });
            }
        });
    });
</script>
