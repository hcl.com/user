<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ include file="common.jsp" %>--%>
<%--<script src="${contextPath }/js/commons.js"></script>--%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>首页</title>

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <c:set var="contextPath" scope="application" value="${pageContext.request.contextPath }"></c:set>
    <%--jquery--%>
    <script  src="../../plugins/jquery-3.2.1/jquery-3.2.1.min.js"></script>

    <%--bootstrap--%>
    <script  src="${contextPath }/plugins/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link href="${contextPath }/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="${contextPath }/plugins/layui-v2.1.5/layui/layui.js"></script>
    <link href="${contextPath }/plugins/layui-v2.1.5/layui/css/layui.css" rel="stylesheet"></link>


    <script src="${contextPath }/js/commons.js"></script>
</head>
<body>
<div class="container" ng-app="app1" ng-controller="controller1">
    <div class="row">
        <fieldset>
            <legend>一级菜单查询</legend>
            <div>
                <form id="searchForm" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-lg-3" style="float:left;">
                            <input type="text" class="form-control" id="keyword"
                                   name="keyword" placeholder="请输入查询条件" autocomplete="off" ng-model="keyword"/>
                        </div>
                        <button ng-click="search()" type="button" id="btnSearch" class="btn btn-default"
                                style="float:left;margin-left:10px;">
                            <i class="fa fa-search"></i> 查询
                        </button>
                        <button ng-click="add()" type="button" id="btnCreate" name="btnCreate"
                                class="btn btn-info" style="float:left;margin-left:10px;"><i
                                class="fa fa-plus"></i> 新增
                        </button>
                        <img class="imgLoading" src="${contextPath }/image/loading_small.gif" hidden="hidden"/>
                    </div>
                </form>
                <table class="table table-hover table-striped table-bordered" width="100%">
                    <thead>
                    <tr style="background-color:#428bca;color:#FFF;">
                        <th class="td_center" style="width:8%;">菜单编号</th>
                        <th class="td_center" style="width:8%;">菜单类型</th>
                        <th class="td_center" style="width:8%;">图标</th>
                        <th class="td_center" style="width:8%;">菜单名称</th>
                        <th class="td_center" style="width:8%;">菜单地址</th>
                        <th class="td_center" style="width:8%;">排序字段</th>
                        <th class="td_center" style="width:8%;">所属范围</th>
                        <th class="td_center" style="width:28%;">操作</th>
                    </tr>
                    </thead>
                    <tbody id="tbody">
                    </tbody>
                </table>
                <div id="demo1"></div>
            </div>
        </fieldset>
    </div>
</div>

<!-- 按钮触发模态框 -->
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">开始演示模态框</button>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">模态框（Modal）标题</h4>
            </div>
            <div class="modal-body">在这里添加一些文本</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary">提交更改</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</body>

<script>
    var scope;
    angular.module('app1', []).controller('controller1', function($scope) {
        alert(1)
        scope = $scope;
        $scope.add = function () {
            console.log(234);
        };
        $scope.search = function () {
            alert(asdf)
        };
    })
    var pageSize = 1;
    var pageIndex = 0;
    var load = false;
    var element;
//    var laypage;
//    var layer;
    var form1;
    layui.use(['laypage', 'table','layer','form'], function () {
        var table = layui.table;
        laypage = layui.laypage;
        layer=layui.layer;
        form1=layui.form;

        //自定义验证规则
        form1.verify({
            title: function(value){
                if(value.length < 5){
                    return '标题至少得5个字符啊';
                }
            }
        });

        //监听提交
        form1.on('submit(demo1)', function(data){
            layer.alert(JSON.stringify(data.field), {
                title: '最终的提交信息'
            })
            return false;
        });

        Hide('form1')
        IdClick('btnCreate', function () {
//            LoadUrl('insertFunction')
        })

        IdClick('btnSearch', function () {
//            toastrSuccess('shousuo')
//            $('#levelModal').modal('show')
        })

        Click('btnGb', function () {
//            layer.clean();
        })

        getData(0)

        function getData(pageIndex) {
            var data = {};
            var keyword = Val('keyword');
            if (keyword && keyword.length > 0) {
                data.keyword = keyword;
            }
            data.pageSize = pageSize;
            data.pageIndex = pageIndex;
            data.pId=0;
            console.log('data',data);
            $.get('api/function/findByPage', data, function (r) {
                if (r.code == 200) {
                    var html = '';
                    $.each(r.data, function (i, v) {
                        html += '<tr>';
                        html += '<td class="td_center">' + v.id + '</td>';
                        html += '<td class="td_center">一级菜单</td>';
                        html += '<td class="td_center"><i class="' + v.icon + '"></i></td>';
                        html += '<td class="td_center">' + v.title + '</td>';
                        html += '<td class="td_center">' + v.url + '</td>';
                        html += '<td class="td_center">' + v.rank + '</td>';
                        html += '<td class="td_center">' + (v.type == 0 ? '终极管理员' : '用户') + '</td>';
                        html += '<td class="td_center">' +
                            '<button name="update" onclick="update(v)" class="layui-btn"><i class="fa fa-pencil fa-fw"></i>  修改</button>' +
                            '<button name="delete" class="layui-btn layui-btn-danger"><i class="fa fa-trash-o fa-fw"></i>  删除</button></td>';
                        html += '<tr>'
                    })
                    Id('tbody').html(html);

                    $("button[name='update']").click(function (v) {
                        consoleLog('update', v)
//                        toastrSuccess(v)
                    })
                    $("button[name='delete']").click(function (v) {
                        toastrSuccess(v)
                    })

                    createPage(r.total);
                } else {
                    toastrError(r.message);
                }
            })
        }

        function createPage(total) {
            if (load) {
                return;
            }
            load = true;
//            layout:count（总条目输区域）、prev（上一页区域）、page（分页区域）、next（下一页区域）、limit（条目选项区域）、 、skip（快捷跳页区域）
            laypage.render({
                elem: 'demo1',
                layout:['count','prev', 'page', 'next','limit','skip'],
                limit:pageSize,
                theme:'#3ac3c0',
                count: total, //数据总数
                jump: function (obj, first) {
                    var index=obj.curr-1;
                    if(first){
                        //第一次不做加载
                    }else{
                        getData(index)
                    }
                },
                hash:function (a,b,c) {
                    consoleLog('a',a)
                    consoleLog('b',b)
                }
            });
        }
//        var layer = layui.layer;
        //--------------方法渲染TABLE----------------
    });


    $(function () {

    })
</script>
</html>
