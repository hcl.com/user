<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="common.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <%
        String scheme = "http://";
        String path = request.getContextPath();
        String host = request.getServerName();
        String port = ":" + request.getServerPort();
        if (host.contains("v-ka.com") || host.contains("vi-ni.com")) {
            scheme = "https://";
            port = "";
        }

        String basepath = scheme + host + port + path;
    %>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>登录</title>

</head>
<body>
<div class="container">
    <form class="col-md-4 form-horizontal" id="loginForm">
        <div class="form-group"><h3 class="form-title">后台管理系统</h3></div>
        <div class="form-group">
            <input class="form-control" type="text" placeholder="账号" id="username" name="username"
                   autofocus="autofocus" maxlength="20"/>
        </div>
        <div class="form-group">
            <input class="form-control required" type="password" placeholder="密码" id="password" name="password"
                   maxlength="8"/>
        </div>
        <div class="form-group">
            <label><a href="javascript:;" id="register_btn" class="">注册用户</a></label>
        </div>
        <div class="form-group">
            <label><input type="checkbox">记住我</label>
        </div>
        <div class="form-group">
            <input type="submit" id="login" class="btn btn-success pull-left" value="登录"/>
        </div>
    </form>
</div>
</body>
</html>
<script type="text/javascript">
    $(function () {

        $('#loginForm').bootstrapValidator({
            live: 'disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            submitHandler: function (validator, form, submitButton) {
                var data = {};
                data.name = $("#username").val();
                data.password = $("#password").val();
                <%--showObjSuccess(<% basepath %>);--%>
                $.post('./user/login', data, function (r) {
                    if (r.code == 200) {
                        top.location = '<%= basepath %>/lteIndex';
                    } else {
                        alert(r.message);
                    }
                })
            },
            fields: {
                username: {
                    validators: {
                        notEmpty: {
                            message: '姓名不能为空'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: '卡号不能为空'
                        },
//                        money: {
//                            message: '金额格式不正确'
//                        }
                    }
                },
            }
        });
    });
    <%--top.location='<%= basepath %>admin/index';--%>
</script>