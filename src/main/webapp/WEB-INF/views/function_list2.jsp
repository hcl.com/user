<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

 <div class="container">
	<div class="row">
		<fieldset>
			<legend>一级菜单查询</legend>
			<div>
				<form id="searchForm" class="form-horizontal">
					<div class="form-group">
						<div class="col-lg-3">
							<input type="text" class="form-control" id="keyword"
								name="keyword" placeholder="请输入查询条件" autocomplete="off"/>
						</div> 
						<button type="button" id="btnSearch" class="button  button-pill button-flat-primary" style="float:left;margin-left:10px;">
							<i class="fa fa-search"></i> 查询</button>
						<button type="button" id="btnCreate" name="btnCreate" class="button button-pill button-flat-primary" style="float:left;margin-left:10px;"><i class="fa fa-plus"></i> 新增</button>
					</div>
				</form>
				<table id="table"
					class="table table-hover table-striped table-bordered">
					<thead>
						<tr  style="background-color:#428bca;color:#FFF;">
							<th style="width:2%;">#</th>
							<th class="td_center" style="width:8%;">菜单编号</th>
							<th class="td_center" style="width:8%;">菜单功能名称</th>
							<th class="td_center" style="width:8%;">排序字段</th>
							 <th class="td_center" style="width:8%;" >所属范围</th> 
							<th class="td_center" style="width:28%;">操作</th>
						</tr>
					</thead>
					<tbody id="tbody">
					</tbody>
				</table>
				<ul class="pagination">
				</ul>
			</div>
		</fieldset>
	</div>
</div>


<!-- Modal -->

<div class="modal fade" id="functionAddModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:50px;" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
		    <form id="functionAddForm" class="form-horizontal">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" acid="10050102"  aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">新增一级菜单</h4>
			      </div>
			      <div class="modal-body">
			      		

							<div class="alert alert-success" style="display: none;"></div>
					<input type="hidden" id="pId"  value=0 />
					<input type="hidden" id="pCode"  value=0 />
                     <div class="form-group">
						<label class="col-md-3 control-label">功能菜单编号</label>
						<div class="col-md-4">
							<input type="text" class="form-control" id="addCode" name="addCode"
								readonly="true" autocomplete="off"/>
						</div>
					</div>
 
					<div class="form-group">
						<label class="col-md-3 control-label">功能菜单名称</label>
						<div class="col-md-4">
							<input type="text" class="form-control" id="addTitle"
								name="addTitle"  autocomplete="off"/>
						</div>
					</div> 
					<div class="form-group">
						<label class="col-md-3 control-label">功能菜单图标</label>
						<div class="col-md-4">
							<input onfocus="text'" class="form-control" id="addIcon"
								name="addIcon"  autocomplete="off"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">排序序号</label>
						<div class="col-md-4">
							<input onfocus="text'" class="form-control" id="addOrder"
								name="addOrder" autocomplete="off"/>
						</div>
					</div>

					<!--0:总店 1:分店 2:总店分店 3:澄泓-->
                   <div class="form-group">
						<label class="col-lg-3 control-label">请输入范围</label>
						<div class="col-md-4">
							<select class="form-control" id="addType" name="addType">   
								 <option value="0">总店 </option>
								 <option value="1">分店 </option>
								 <option value="2">总店分店 </option>
								 <option value="3">澄泓 </option>
                                 <option value="4">代理 </option>
						    </select>	
						</div>
					</div>
						
			     

			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			        <button type="submit" id="btnUpdate" acid="10050102" class="button button-rounded button-flat-primary"> 新增</button>
			      </div>
			      <input type="hidden" id="inputRole" class="inputRole" />
		      </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->





<!-- END PAGE CONTENT-->
<script type="text/javascript">
$(function() { 
	var reLoad = false;
	var pageSize = 10;
	var keyword = '';
	fetchData(0);
	document.onkeydown = function(e){
		    var ev = document.all ? window.event : e;
		    if(ev.keyCode==13) {
		    	$('#btnSearch').click();
		    	e.preventDefault();
		    }
		};
		$('#btnSearch').click(function(){ 
			keyword = $('#keyword').val();
			reLoad = false;
			
			if(keyword.length > 0) {
				fetchData(0,keyword);
			}else {
				fetchData(0);
			}
		});
	function fetchData(pageIndex, keyword) {
		var function_list = eval("("+sessionStorage.function_list+")");
		if(function_list) {
			pageIndex = function_list.pageIndex;
			keyword = function_list.keyword;
			sessionStorage.removeItem("function_list");
		}
		$('.imgLoading').attr('hidden', false); 
		var function_list_url = './api/function/findByPage?pageSize='+pageSize+'&pageIndex='+pageIndex+'&pId='+0;
		if(keyword && keyword.length > 0) {
			function_list_url += "&keyword=" + keyword;
		}

		$.ajax({
			url : function_list_url ,
			type : 'GET',
			success : function(result) {
				window.function_list_url = "";
				$('.imgLoading').attr('hidden', true); 
				var html = '';var total = result.total; 
				for(var i = 0; i < result.data.length; i++) {
				    var row = result.data[i];
				    
					html += '<tr>';
					html += '<td class="td_center"  >'+(i+1)+' </td>';
					html += '<td class="td_center" id="id'+row.id+'">'+row.id+'</td>';
					html += '<td class="td_center" id="title'+row.id+'">'+row.title+'</td>';
					html += '<td class="td_center"  id="order'+row.id+'">'+row.order+'</td>'; 
					//0:总店 1:分店 2:总店分店 3:澄泓
					if(row.type==0){
						html += '<td class="td_center"id="type'+row.id+'" data-type="'+row.type+'">总店</td>';  
					}
					if(row.type==1){
						html += '<td class="td_center"id="type'+row.id+'" data-type="'+row.type+'">分店</td>';  
					}
					if(row.type==2){
						html += '<td class="td_center" id="type'+row.id+'" data-type="'+row.type+'">总店分店</td>';  
					}
					if(row.type==3){
						html += '<td class="td_center"id="type'+row.id+'" data-type="'+row.type+'">澄泓</td>';  
					}
					if (row.type == 4) {
					    html += '<td class="td_center"id="type' + row.id + '" data-type="' + row.type + '">代理</td>';
					}
				    html += '<td class="td_center">';
				    //查询二级菜单
				    html += '<button class="button button-pill button-flat-highlight button-small"' + 'name="btn_config" data-id="'+row.id+'" pId="'+row.pId+'" code="'+row.code+'"><span class="glyphicon glyphicon-repeat"></span>&nbsp;二级菜单管理</button>&nbsp;';	
				    html += '<button class="button button-pill button-flat-primary button-small " logo="'+row.logo+'" ';
				    
				    html += 'name="btn_upd" ';
				    html +=  'data-id="'+row.id+'"    pId="'+row.pId+'" p_code="'+row.pCode+'" code="'+row.code+'" url="'+row.url+'"    icon="'+row.icon+'" order="'+row.order+'" type="'+row.type+'" ><span class="glyphicon glyphicon-edit"></span>&nbsp;修改</button>';	
				    html += '&nbsp;<button class="button button-pill button-flat-highlight button-small" name="btn_del" data-id="'+row.id+'"><i class="fa fa-trash-o"></i> 删除</button>';

				    html += '</td></tr>';
				} 
				$("#tbody").html(html);
				if(!reLoad) createPage(total,pageIndex);
				
				//删除菜单操作
				$(":button[name='btn_del']").click(function(event) {
					
					if(!confirm("是否确认删除?")) return;
					
					$('.imgLoading').attr('hidden', false);
					
					var id = $(this).attr('data-id');
					
					$.ajax({
						url : './api/function/'+id,
						type : 'DELETE',
						beforeSend: function(xhr) {  
						    xhr.setRequestHeader("Accept", "application/json");  
						    xhr.setRequestHeader("Content-Type", "application/json");  
						},  
						success : function(result) {
							
							$('.imgLoading').attr('hidden', true);
							if(result.code == SUCCESS) {
								$.scojs_message('删除成功!', $.scojs_message.TYPE_OK);
								reLoad = false;
								sessionStorage.function_list=JSON.stringify({pageIndex:pageIndex});
								fetchData(0, keyword);
							}
							else {
								$.scojs_message('删除失败!', $.scojs_message.TYPE_ERROR);
							}
						},
						error: function(result){
							$.scojs_message('网络错误', $.scojs_message.TYPE_ERROR);
							$('.imgLoading').attr('hidden', true); 
						}
					})
					
				});
				
				$(":button[name='btn_upd']").click(function(event){
						sessionStorage.function_list=JSON.stringify({pageIndex:pageIndex, keyword:keyword});
						
				    	var id = $(this).attr('data-id'); 
				  
				       var pId=$(this).attr('pId');
				       
				       var  pCode=$(this).attr('p_code');
				       var code=$(this).attr('code');
				       var url=$(this).attr('url');
				       var icon=$(this).attr('icon');
				       
						var title = $("#title"+id).html();
						var order=$("#order"+id).html();
						//var type=$("#type"+id).html();
						var type = $("#type"+id).attr('data-type');
						
						$("#main-content").html(''); 
						$("#main-content").load('./admin/function_edit',function() {
							$('#id').val(id);
						    $('#pId').val(pId);
						    $('#pCode').val(pCode);
						    $('#code').val(code);
						    $('#url').val(url);
						    $('#icon').val(icon);
							$('#title').val(title);
							$('#order').val(order);
							$('#type').val(type);
						});
						event.preventDefault();  
					});
				//查询二级菜单
				$(":button[name='btn_config']").click(function(event){
					sessionStorage.function_list=JSON.stringify({pageIndex:pageIndex, keyword:keyword});
					event.preventDefault(); 
					 var id = $(this).attr('data-id'); 
					 var pId = $(this).attr('data-id'); 
					 var type = $("#type"+id).attr('data-type');
					 var pCode = $(this).attr('code')
					 var url = './admin/secondFunction_list?pId='+id+'&code='+pCode;
					var url = sessionStorage.secondFunction_list_url=url;
					$("#main-content").html(''); 
					$("#main-content").load(url, function() {
						$('#addPId').val(id);
						$('#addPcode').val(pCode);
						$('#addType').val(type);
					});
					
					
				});
				
			}
		});
		
	}   
	//新增按钮
	$(":button[name='btnCreate']").click(function(event){
        $('#functionAddModal').modal('show');

        fetchcodeData();


    });
    //新增一级菜单function
    function fetchcodeData() {
		$('.imgLoading').attr('hidden', false); 
		var url = './api/function/findMaxCode';
		$.ajax({
			url : url ,
			type : 'GET',
			success : function(result) {
				$('.imgLoading').attr('hidden', true); 
				var code=result.data;
				 $('#addCode').val(parseInt(code)+1);
				
			}
		});
	}
	$('#functionAddForm').bootstrapValidator({
		live : 'disabled',
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		submitHandler: function(validator, form, submitButton) {
        	 var data = {};
    	     data.pId = $('#pId').val();
             data.pCode = $('#pCode').val();
             data.code = $('#addCode').val();
             data.title = $('#addTitle').val();
             data.icon = $('#addIcon').val();
             data.order = $('#addOrder').val();
             data.type = $('#addType').val();
        	

			$.post('./api/function', data).done(function(result){
				if(result.code == SUCCESS) {
					clearUpdateForm();
					$('#functionAddModal').modal('hide');
					fetchData(0);
					$.scojs_message('添加菜单成功!', $.scojs_message.TYPE_OK);
					

				}
				else {
					$.scojs_message('菜单添加失败!', $.scojs_message.TYPE_ERROR);  
				}	       	

			});
        },
		fields : {
        	addCode: {
        		validators: {
	                notEmpty: {
	                    message: '功能菜单编号不能为空'
	                }
                }
        		
                
        	},
        	addTitle: {
        		validators: {
	                notEmpty: {
	                    message: '功能名称不能为空'
	                }
                }
        	},
        	addIcon: {
        		validators: {
	                notEmpty: {
	                    message: '功能图标名称不能为空'
	                }
                }
        	},
        	addOrder: {
        			validators: {
	                notEmpty: {
	                    message: '排序字段不能为空'
	                }
                }	
        	},
        	addType: {
        		validators: {
	                notEmpty: {
	                    message: '范围不能为空'
	                }
                }

        	}
        }
	});
	function clearUpdateForm() {
		$('#functionAddForm').data('bootstrapValidator').resetForm(true);
	}

	function createPage(total,pageIndex2) {
		reLoad = true;
		$(".pagination").jBootstrapPage({
			pageSize : pageSize,
			total : total,
			maxPageButton:10,
			selectedIndex:pageIndex2+1,
			onPageClicked: function(obj, pageIndex) {
				fetchData(pageIndex,keyword);
			}
		});
	}
	
});
</script>