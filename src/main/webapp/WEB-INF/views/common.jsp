<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="contextPath" scope="application" value="${pageContext.request.contextPath }"></c:set>

<%--jquery--%>
<script  src="../../plugins/jquery-3.2.1/jquery-3.2.1.min.js"></script>

<%--tether--%>
<%--<link href="${contextPath }/plugins/tether1.3.6/tether.min.css" rel="stylesheet">--%>
<%--<script src="${contextPath }/plugins/tether1.3.6/tether.min.js"></script>--%>

<%--bootstrap--%>
<script  src="${contextPath }/plugins/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<link href="${contextPath }/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">

<%--bootstrapValidator--%>
<link href="${contextPath }/plugins/bootstrapvalidator-0.4.5/vendor/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<script src="${contextPath }/plugins/bootstrapvalidator-0.4.5/dist/js/bootstrapValidator.min.js"></script>

<%--jBootsrapPage--%>
<link href="${contextPath }/plugins/jBootsrapPage/jBootsrapPage.css" rel="stylesheet"/>
<script src="${contextPath }/plugins/jBootsrapPage/jBootstrapPage.js"></script>

<%--引入toastr--%>
<link href="${contextPath }/plugins/toastr/toastr.css" rel="stylesheet"/>
<script src="${contextPath }/plugins/toastr/toastr.js"></script>

<%--引入fontawesome--%>
<link href="${contextPath }/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">

<%--angular--%>
<%--<script src="${contextPath }/js/angular.min.js"></script>--%>

<%--layui--%>
<script src="${contextPath }/plugins/layui-v2.1.5/layui/layui.js"></script>
<link href="${contextPath }/plugins/layui-v2.1.5/layui/css/layui.css" rel="stylesheet"></link>

<script src="${contextPath }/js/commons.js"></script>
<script type="text/javascript">

    $(function () {
        toastr.options = {
            "closeButton": false, //是否显示关闭按钮--%>
            "debug": false, //是否使用debug模式--%>
            "positionClass": "toast-top-right",//弹出窗的位置
            "showDuration": "300",//显示的动画时间--%>
            "hideDuration": "1000",//消失的动画时间--%>
            "timeOut": "3000", //展现时间
            "extendedTimeOut": "1000",//加长展示时间--%>
            "showEasing": "swing",//显示时的动画缓冲方式--%>
            "hideEasing": "linear",//消失时的动画缓冲方式--%>
            "showMethod": "fadeIn",//显示时的动画方式--%>
            "hideMethod": "fadeOut" //消失时的动画方式--%>
        };

        function lyMsg(r) {
            if(r.message){

                ly.msg(r.message)

            }else{
                ly.msg(JSON.stringify(r))
            }
        }
    });


</script>