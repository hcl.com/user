<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="common.jsp" %>
<%--<script src="${contextPath }/js/commons.js"></script>--%>
<!DOCTYPE html>
<html lang="zh-CN">
<style>

    ul {
        list-style-type: none;
    }

    a {
        color: #b63b4d;
        text-decoration: none;
    }

    .a_menu:hover {
        color: #b63b4d;
        text-decoration: none;
    }

</style>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>首页</title>
    <%--<link href="//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">--%>
</head>
<body>

<div class="col-lg-2 panel-group" id="accordion" style="margin-top: 5px;">
</div>
<div class="page-content-wrapper">
    <div id="main-content" class="page-content">
    </div>
</div>
<iframe class="col-lg-10" id="right" runat="server" width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="padding-top: 10px;"></iframe>
<div id="divContent"></div>
<%--<i class="fa fa-address-book"></i>--%>
</body>
</html>
<script type="text/javascript">
    $(function () {
        getData()

        function getData() {
            $.get('api/function/list', function (r) {
                if (r.code == 200) {
                    consoleLog("数据", r)
                    var h = ''
                    Id('accordion').empty()
                    $.each(r.data, function (i, v) {
                        h += '<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title"><i class="'+v.icon+'"></i><a data-toggle="collapse"'
                        h += 'data-parent="#accordion" class="a_menu1"'
                        h += ' href="#collapse'+i+'">&nbsp;&nbsp;' + v.title + '</a>';
                        h += ' </h4></div><div id="collapse'+i+'" class="panel-collapse collapse"><div class="panel-body"><ul>'

                        $.each(v.functions, function (i1, v1) {
                            h += '<li><a href="' + v1.url + '" class="son_menu"><i class="' + v1.icon + '"></i>&nbsp;&nbsp;' + v1.title + '</a></li>'
                        })

                        h += '</ul></div></div></div>'

                    })
                    Id('accordion').html(h)
                    consoleLog("html", h)

                    $('.son_menu').click(function (e) {
                        var url = $(this).attr('href')
                        consoleLog(url)
//                        $('#right').attr('src', url)

                        $('#divContent').html('');
                        $("#divContent").load(url);
                        e.preventDefault()
                    })

                    function hideMenu(v) {
                        $('.panel-collapse').each(function () {
                            var id = $(this).attr('id')
                            if (('#' + id) == v) {
                                $(this).collapse('show')
                            } else {
                                $(this).collapse('hide')
                            }
                        })
                    }

                    $('.a_menu1').click(function (e) {
                        var son = $(this).attr('href')
                        hideMenu(son)
                    })
                } else {
                    alert(r.message)
                }
            });
        }


    });
</script>
