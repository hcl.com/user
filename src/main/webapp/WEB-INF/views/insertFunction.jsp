<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ include file="common.jsp" %>--%>
<%--<script src="${contextPath }/js/commons.js"></script>--%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>新建菜单</title>

</head>
<body>
<input hidden="hidden" id="fId"/>
新建菜单
<select>
    <option>a</option>
    <option>aasf</option>
</select>
<hr>
<form class="layui-form" id="form1">

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">名称</label>
        <div class="layui-input-block">
            <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">地址</label>
        <div class="layui-input-block">
            <input type="text" name="url" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">图标</label>
        <div class="layui-input-block">
            <input type="text" name="icon" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">排序</label>
        <div class="layui-input-block">
            <input type="text" name="rank" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">范围</label>
        <div class="layui-input-block">
            <select name="type" lay-verify="interest" lay-filter="select1">
                <option value=""></option>
                <option value="0">终极管理员</option>
                <option value="1">用户</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item layui-col-md-offset2">
        <div class="layui-input-block">
            <button id='btnSub' class="layui-btn" lay-submit="" lay-filter="demo1">提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            <button type="button" id="btnGb" class="layui-btn layui-btn-normal">返回</button>
        </div>
    </div>
</form>
</body>

<script>
    var aId = '${param.id}';
    $(function () {
        layui.use(['form'], function () {
            var form1 = layui.form;

            //自定义验证规则
            form1.verify({
                title: function (value) {
                    if (value.length < 1) {
                        return '不能为空';
                    }
                }, interest: function (val) {
                    if (!val) {
                        return '请选择';
                    }
                }
            });

            //监听提交
            form1.on('submit(demo1)', function (data) {
                layer.alert(JSON.stringify(data.field), {
                    title: '最终的提交信息'
                })
                return false;
            });

            form1.render('select', 'select1');
//        var layer = layui.layer;
            //--------------方法渲染TABLE----------------
        });
        toastrSuccess(aId)
        if (aId) {
            //修改
        } else {
            //新增
        }
    })
</script>
</html>
