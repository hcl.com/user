<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="common.jsp" %>
<%--<script src="${contextPath }/js/commons.js"></script>--%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>首页</title>

</head>
<body>


<form class="layui-form" action="" id="form1">
    <div class="layui-input-block"></div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">父菜单</label>
        <div class="layui-input-block">
            <select name="interest" ng-model="pId" lay-filter="aihao" lay-verify="interest">
            </select>
        </div>
    </div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">名称</label>
        <div class="layui-input-block">
            <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">地址</label>
        <div class="layui-input-block">
            <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">图标</label>
        <div class="layui-input-block">
            <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">排序</label>
        <div class="layui-input-block">
            <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
        <label class="layui-form-label">范围</label>
        <div class="layui-input-block">
            <select name="interest" lay-filter="aihao" lay-verify="interest">
                <option value="0">写作</option>
                <option value="1">阅读</option>
                <option value="2">游戏</option>
                <option value="3">音乐</option>
                <option value="4">旅行</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item layui-col-md-offset2">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            <button class="layui-btn" id="btn1">关闭弹窗</button>
        </div>
    </div>
</form>
<button class="layui-btn" id="btn">弹窗</button>

</body>

</html>

<script>
    layui.use(['form', 'layedit', 'laydate', 'layer'], function () {
        var form = layui.form
            , layer = layui.layer
            , layedit = layui.layedit
            , laydate = layui.laydate, layer = layui.layer;

        $("#btn").click(function () {
            layer.open({
                type: 1,
                title:'asdf',
                area: ['820px', '640px'], //宽高
                content: $('#form1'),
                end: function () {
                }
            });
        })
        $("#btn1").click(function () {
            layer.closeAll('dialog');
        })

        //自定义验证规则
        form.verify({
            title: function (value) {
                if (!value) {
                    return '空';
                }
            }
            , pass: [/(.+){6,12}$/, '密码必须6到12位']
            , content: function (value) {
                layedit.sync(editIndex);
            }, interest: function (val) {
                if (!val) {
                    return '未选中';
                }
            }
        });


        //监听提交
        form.on('submit(demo1)', function (data) {
            layer.alert(JSON.stringify(data.field), {
                title: '最终的提交信息'
            })
            return false;
        });


    });
</script>
