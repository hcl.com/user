<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="common.jsp" %>
<%--<script src="${contextPath }/js/commons.js"></script>--%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>首页</title>

</head>
<body>
<div class="container" style="background: red;">
    <%--获取父id--%>
    <input id="pId" hidden="hidden">
    <div class="row">
        <fieldset>
            <legend>二级菜单查询</legend>
            <div>
                <form id="searchForm" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-lg-3" style="float:left;">
                            <input type="text" class="form-control" id="keyword"
                                   name="keyword" placeholder="请输入查询条件" autocomplete="off" ng-model="keyword"/>
                        </div>
                        <button ng-click="search()" type="button" id="btnSearch" class="btn btn-default"
                                style="float:left;margin-left:10px;">
                            <i class="fa fa-search"></i> 查询
                        </button>
                        <button ng-click="add()" type="button" id="btnCreate" name="btnCreate"
                                class="btn btn-info" style="float:left;margin-left:10px;"><i
                                class="fa fa-plus"></i> 新增
                        </button>
                        <img class="imgLoading" src="${contextPath }/image/loading_small.gif" hidden="hidden"/>
                    </div>
                </form>
                <table id="table"
                       class="table table-hover table-striped table-bordered">
                    <thead>

                    <tr style="background-color:#428bca;color:#FFF;">
                        <th style="width:2%;">#</th>
                        <th class="td_center" style="width:8%;">菜单编号</th>
                        <th class="td_center" style="width:8%;">菜单类型</th>
                        <th class="td_center" style="width:8%;">图标</th>
                        <th class="td_center" style="width:8%;">菜单名称</th>
                        <th class="td_center" style="width:8%;">菜单地址</th>
                        <th class="td_center" style="width:8%;">排序字段</th>
                        <th class="td_center" style="width:8%;">所属范围</th>
                        <th class="td_center" style="width:28%;">操作</th>
                    </tr>
                    </thead>
                    <tbody id="tbody">
                    <tr ng-repeat="x in datas">
                        <td>{{$index+1}}</td>
                        <td>{{x.id}}</td>
                        <td><span ng-if="x.pId==0">一级菜单</span><span ng-if="x.pId!=0">二级菜单</span></td>
                        <td><i class="{{x.icon}}"/></td>
                        <td>{{ x.title }}</td>
                        <td>{{x.url}}</td>
                        <td>{{x.rank}}</td>
                        <td ng-switch="x.type">
                                    <span ng-switch-when="0">
                                        终极管理员
                                    </span>
                            <span ng-switch-when="1">
                                        用户
                                    </span>
                        </td>
                        <td>
                            <button ng-click="upd(x)" class="btn btn-info"><i class="fa fa-edit"></i>修改</button>
                            <button ng-click="del(x.id)" class="btn btn-danger"><i class="fa fa-trash-o"></i>删除</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <ul class="pagination">
                </ul>
            </div>
        </fieldset>
    </div>

    <form class="layui-form" id="form1">
        <div class="layui-input-block"></div>

        <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
            <label class="layui-form-label">父菜单</label>
            <div class="layui-input-block">
                <select name="pId" ng-model="pId" lay-filter="pId"  lay-verify="interest" ng-model="pId"
                        ng-options="y.title for (x,y) in pDatas">
                </select>
            </div>
        </div>

        <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
            <label class="layui-form-label">名称</label>
            <div class="layui-input-block">
                <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入"
                       class="layui-input">
            </div>
        </div>

        <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
            <label class="layui-form-label">地址</label>
            <div class="layui-input-block">
                <input type="text" name="url" lay-verify="title" autocomplete="off" placeholder="请输入"
                       class="layui-input">
            </div>
        </div>

        <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
            <label class="layui-form-label">图标</label>
            <div class="layui-input-block">
                <input type="text" name="icon" autocomplete="off" placeholder="请输入"
                       class="layui-input">
            </div>
        </div>

        <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
            <label class="layui-form-label">排序</label>
            <div class="layui-input-block">
                <input type="text" name="rank" lay-verify="title" autocomplete="off" placeholder="请输入"
                       class="layui-input">
            </div>
        </div>

        <div class="layui-form-item layui-col-md4 layui-col-md-offset3">
            <label class="layui-form-label">范围</label>
            <div class="layui-input-block">
                <select name="type" lay-filter="type" lay-verify="interest">
                    <option value="0">终极管理员</option>
                    <option value="1">用户</option>
                </select>
            </div>
        </div>

        <div class="layui-form-item layui-col-md-offset2">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                <button class="layui-btn" id="btn1">关闭弹窗</button>
            </div>
        </div>
    </form>
</div>

</body>

</html>

<script type="text/javascript">
    var pId = '${param.pId}';
    var pageSize = 4;
    var scope;
    var reLoad = false;
    var ly;
    var id;
    var html;
    var model;
    $(function () {
//        html = $('#form1').html();
//        $('#form1').hide()
    })

    var layer;
    layui.use(['form', 'layer'], function () {
        var form = layui.form;
        layer = layui.layer;

        $("#btnCreate").click(function () {
            model = layer.open({
                type: 1,
                title: 'asdf',
                area: ['820px', '640px'], //宽高
                content: $('#form1'),
                end: function () {

                }
            });
        })

        $("#btn1").click(function () {
            consoleLog('model', model)
            layer.close(model);
        })

        //自定义验证规则
        form.verify({
            title: function (value) {
                if (!value) {
                    return '空';
                }
            }, interest: function (val) {
                if (!val) {
                    return '未选中';
                }
            }
        });

        //监听提交
        form.on('submit(demo1)', function (result) {
            var data = result.field;
            var url='';
            var msg=''
            if (id) {
                //修改
                url='api/function/update'
                msg='修改成功'
            } else {
                data.pId = scope.pId.id;
                url='api/function'
                msg='新增成功'
            }
            consoleLog('提交', data)
            $.post(url, data, function (r) {
                if (r.code == 200) {
                    toastr.success(msg)
                    scope.getData(0, '')
                    return true;
                } else {
                    toastr.error(r.message)
                    return false;
                }
            })
        });

    });

    angular.module('app1', []).controller('controller1', function ($scope, $http, $location) {
//        pId = $location.search().pId;
        scope = $scope;
        scope.pDatas = []

//        fetchData(0, '',$scope);
        $scope.search = function () {
            reLoad = false;
            $scope.getData(0, $scope.keyword)
        }

        $scope.del = function (id) {
            if (window.confirm('确认删除？')) {
                $.ajax({
                    type: "delete",
                    url: 'api/function/' + id,
                    success: function (r) {
                        if (r.code == 200) {
                            toastr.success('删除成功')
                            scope.getData(0, '')
                        } else {
                            toastr.error(r.message)
                        }
                    }
                });
            }
        }

        scope.getFrom = function () {
            var data = {};
            data.id = id;
            data.pId = scope.pId;
            data.title = scope.title;
            data.url = scope.url;
            data.icon = scope.icon;
            data.rank = scope.rank;
            data.type = scope.type;
            if (data.pId) {

            }
            return data;
        }

        scope.md_sub = function () {
            var data = scope.getFrom();
            if (!data.ok) {
                return;
            }
//
        }

        $scope.upd = function (r) {
            id = r.id;
            var pId = r.pId;
            //一级菜单
            $.attr(scope.pId, function (i, v) {
                if (v.id == pId) {
                    scope.pId = v;
                }
            })
            scope.title = r.title
            scope.icon = r.icon
            scope.rank = r.rank
            scope.url = r.url
            //标题
            $('#myModalLabel').html('修改菜单')
            $('#md_sub').html('修改1')
            $('#functionAddModal').modal('show');

            model = layer.open({
                type: 1,
                title: 'asdf',
                area: ['820px', '640px'], //宽高
                content: $('#form1'),
                end: function () {

                }
            });
        }

        $scope.getpData = function () {
            var data = {}
//            data.pId=pId;
            data.pageIndex = 0;
            data.pageSize = 99;
            $.get('api/function/findByPage', data, function (r) {
                if (r.code == 200) {
                    if (r.data.length > 0) {
                        console.log('父菜单数据', r.data)
                        $.each(r.data, function (i, v) {
                            console.log('子菜单数据', v.pId)
                            if (v.pId == '0') {
                                console.log('符合', v)
                                $scope.pId = v
                                $scope.pDatas.push(v);

                                html = $('#form1').html();
                                $('#form1').hide()
                            }
                        })
                        console.log('筛选后菜单数据', scope.pDatas)
                    } else {
                        toastr.error('数据为空')
                    }
                } else {
                    toastr.error(r.message)
                }
            })
        }
        $scope.getpData();

        $scope.getData = function (pageIndex, keyword) {
            var data = {};
            data.pageIndex = pageIndex;
            data.pId = pId;
            data.pageSize = pageSize;
            if (keyword && keyword.length > 0) {
                data.keyword = keyword;
            }
            $http({
                url: 'api/function/findByPage',
                method: 'GET',
                params: data,
            }).then(function (r) {
                if (r.data.code == 200) {
                    $scope.datas = r.data.data;
                    $scope.createPage(r.data.total)
                } else {
                    console.log(r)
                }
            });
        }
        $scope.getData(0);

        $scope.createPage = function (total) {
            console.log('分页')
            if (reLoad) {
                return
            }
            reLoad = true;
            $(".pagination").jBootstrapPage({
                pageSize: pageSize,
                total: total,
                maxPageButton: 10,
                onPageClicked: function (obj, pageIndex) {
                    $scope.getData(pageIndex);
                }
            });
        }
    });

    $('#functionAddForm').bootstrapValidator({
        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function (validator, form, submitButton) {
            var data = scope.getFrom();

            $.post('./api/function', data).done(function (result) {
                if (result.code == SUCCESS) {
                    clearUpdateForm();
                    $('#functionAddModal').modal('hide');
                    scope.getData(0)
                    toastr.success('添加成功')
                }
                else {
                    toastr.success('添加成功' + result.message)
                }
            });
        },
        fields: {
            pId: {
                validators: {
                    notEmpty: {
                        message: '空父菜单'
                    }
                }
            },
            addTitle: {
                validators: {
                    notEmpty: {
                        message: '功能名称不能为空'
                    }
                }
            },
            addUrl: {
                validators: {
                    notEmpty: {
                        message: '空地址'
                    }
                }
            },
            addIcon: {
                validators: {
                    notEmpty: {
                        message: '功能图标名称不能为空'
                    }
                }
            },
            addOrder: {
                validators: {
                    notEmpty: {
                        message: '排序字段不能为空'
                    }
                }
            },
            addType: {
                validators: {
                    notEmpty: {
                        message: '范围不能为空'
                    }
                }

            }
        }
    });

</script>
