function eachHide(o) {
    $(o).each(function (i) {
        $(this).hide();
    });
}

function eachShow(o) {
    $(o).each(function (i) {
        $(this).show();
    });
}

function Hide(o) {
    $('#' + o).hide();
}

function Show(o) {
    $('#' + o).show();
}

/**
 * 获取id
 * @param o
 * @returns {void|jQuery|HTMLElement}
 */
function Id(o) {
    return $('#' + o);
}

/**
 * 为元素添加点击事件
 * @param o
 * @param fun
 * @constructor
 */
function IdClick(o, fun) {
    $('#' + o).click(fun);
}
/**
 * 为元素添加点击事件
 * @param o
 * @param fun
 * @constructor
 */
function Click(o, fun) {
    $('#' + o).click(fun);
}

/**
 * 获取元素的值，或者为元素设置值
 * @param o
 * @param val
 * @returns {jQuery}
 * @constructor
 */
function Val(o, val) {
    if (val) {
        Id(o).val(val);
    } else {
        return Id(o).val();
    }
}

function refresh() {
    $('.selectpicker').selectpicker('refresh');
    $('.selectpicker1').selectpicker('refresh');
}

function consoleLog(title, content) {
    if (content) {
        console.log(title, content);
    } else {
        console.log(title);
    }
}

function showLoading() {
    $('.imgLoading').show();
}

function hideLoading() {
    $('.imgLoading').hide();
}

function toastrSuccess(msg) {
    toastr.success(ToJson(msg));
}

function toastrError(msg) {
    toastr.error(ToJson(msg));
}
function toastrResult(result) {
    toastr.error(result.message);
}
function ToJson(obj) {
    return JSON.stringify(obj)
}
/**
 * 加载内容页面
 * @param url
 * @constructor
 */
function LoadUrl(url) {
    $('#divRight').load(url)
}


